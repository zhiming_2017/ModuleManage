import axios from 'axios'
import qs from 'qs'
import { Message } from 'element-ui'
import {responseHandle} from './responseHandle'
axios.defaults.baseURL = window.ApiUrl
axios.defaults.timeout = 30 * 1000
// 对象转换为formData
const transfromData = object => {
  let ret = new FormData()
  Object.entries(object).forEach(item => {
    ret.append(item[0], item[1])
  })
  return ret
}
export function fetch (options) {
  let data = ''
  if (options.params) data = qs.stringify(Object.assign(options.params))
  if (options.data) data = qs.stringify(Object.assign(options.data))
  return new Promise((resolve, reject) => {
    const instance = axios.create({ // instance创建一个axios实例，可以自定义配置
      headers: {
        'Content-Type': options.typeFormData ? 'application/x-www-form-urlencoded' : 'application/json;charset=UTF-8',
        'Authorization': localStorage.getItem('token')
      },
      responseType: 'json',
      data: options.typeFormData ? transfromData[data] : data
    })
    instance.interceptors.request.use(function (config) {
      config.method = options.method || 'post'
      return config
    }, function (error) {
      Message.error(error)
      return Promise.reject(error)
    })
    instance(options)
      .then(response => { // then请求数据成功后进行操作
        // console.log(response)
        responseHandle(response, resolve)
        // resolve(response) // 把请求的数据发到引用的地方
        if (options.isExport) {
          const blob = new Blob([response.data]) // new Blob([res])中不加data就会返回下图中[objece objece]内容（少取一层）
          if (!!window.ActiveXObject || 'ActiveXObject' in window) { // 兼容IE
            window.navigator.msSaveOrOpenBlob(blob, `${response.config.fileName}`)
          } else {
            const aLink = document.createElement('a')
            aLink.download = `${response.config.fileName}`
            aLink.style.display = 'none'
            aLink.href = URL.createObjectURL(blob)
            document.body.appendChild(aLink)
            aLink.click()
            URL.revokeObjectURL(aLink.href) // 释放URL 对象
            document.body.removeChild(aLink)
          }
        }
      })
      .catch(error => {
        reject(error)
        Message.error('请求超时，稍后重试')
      })
  })
}

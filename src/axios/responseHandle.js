import { Message } from 'element-ui'
import router from '../router'

export const responseHandle = function (res, resolve) {
  if (res.data.code === 200) {
    resolve(res)
  } else if (res.data.code === 500 || res.data.code === 404) {
    Message.error(res.data.msg)
  } else if (res.data.code === 406) {
    router.push('/login')
    Message.error(res.data.msg)
  }
}

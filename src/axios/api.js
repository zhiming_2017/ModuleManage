import {fetch} from './fetch'
export default {
  User: {
    query: data => fetch({url: '/user/query', data}),
    add: data => fetch({url: '/user/add', data}),
    edit: data => fetch({url: '/user/edit', data}),
    del: data => fetch({url: '/user/del', data})
  }
}